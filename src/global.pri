# This is 'global.pri' file which defines
# GLOBAL definitions for the project
# include this file in each "*.pro" file, where it's needed
# USAGE: include(./global.pri)

# VERSION
VERSION = 0.7.2
DEFINES += VERSION=\\\"$$VERSION\\\"

# Define 'MAEMO' specific CONFIG/DEFINE
# To handle 'MAEMO' specific soruces/code
DISTRO = $$system(cat /etc/issue 2>/dev/null || uname -o)
contains( DISTRO, [Mm]aemo ) {
    # for 'MAEMO' specific source code parts
    DEFINES += MAEMO
    # for eg. including 'MAEMO' specific files
    CONFIG += maemo
}

contains( DISTRO, [Ii]nternet ) {
    contains( DISTRO, [Tt]ablet ) {
        # Nokia N810 device
        DEFINES += N810
    }
}

# gcc 4.2.1 on Maemo doesn't understand anything newer then c++98.
# make sure we explicitly add the flag even when building
# with a modern compiler.
# Only under Qt4 because Qt5 needs c++11
unix: {
    equals( QT_MAJOR_VERSION, 4 ) {
        QMAKE_CXXFLAGS += -std=c++98
    }
}
