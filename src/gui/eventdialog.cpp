/*
 * Copyright (C) 2010 Ixonos Plc.
 * Copyright (C) 2011-2024 Philipp Spitzer, gregor herrmann, Stefan Stahl
 *
 * This file is part of ConfClerk.
 *
 * ConfClerk is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * ConfClerk is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * ConfClerk.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "eventdialog.h"
#include "conference.h"

#include <QScrollBar>

#ifdef MAEMO
#include "alarm.h"
#include "appsettings.h"
#endif

QString toHtmlEscaped(const QString& string) {
#if QT_VERSION >= 0x050000
    return string.toHtmlEscaped();
#else
    return Qt::escape(string);
#endif
}

EventDialog::EventDialog(int conferenceId, int eventId, QWidget *parent): QDialog(parent), mConferenceId(conferenceId), mEventId(eventId) {
    setupUi(this);

#ifdef MAEMO
    showFullScreen();
#endif

    Event event = Event::getById(mEventId, mConferenceId);

    QString info;
    // title
    info.append(QString("<h1>%1</h1>\n").arg(toHtmlEscaped(event.title())));

    // persons
    info += QString("<h2>%1</h2>\n").arg(tr("Persons"));
    QStringList persons = event.persons();
    for (int i = 0; i != persons.size(); ++i) persons[i] = toHtmlEscaped(persons[i]);
    info += QString("<p>%1</p>\n").arg(persons.join(", "));

    // abstract
    info += QString("<h2>%1</h2>\n").arg(tr("Abstract"));
    if (Qt::mightBeRichText(event.abstract())) {
        info += event.abstract();
    } else {
        info += Qt::convertFromPlainText(event.abstract(), Qt::WhiteSpaceNormal);
    }

    // description
    info += QString("<h2>%1</h2>\n").arg(tr("Description"));
    if (Qt::mightBeRichText(event.description())) {
        info += event.description();
    } else {
        QString description = Qt::convertFromPlainText(event.description(), Qt::WhiteSpaceNormal);
        // make links clickable
        QRegExp rx("<?(((s?ftp|https?|svn|svn\\+ssh|git|git\\+ssh)://|(file|news):|www\\.)[-a-z0-9_.:%]*[a-z0-9](/[^][{}\\s\"<>()]*[^][{}\\s\"<>().,:!])?/?)>?");
        info += description.replace(rx, "<a href=\"\\1\">\\1</a>");
    }

    // links
    info += QString("<h2>%1</h2>\n<ul>\n").arg(tr("Links"));
    QMapIterator<QString, QString> i(event.links());
    while (i.hasNext()) {
        i.next();
        QString url(i.value());
        QString name(i.key());
        if (url.isEmpty() || url == "http://") continue;
        if (name.isEmpty()) name = url;
        info += QString("<li><a href=\"%1\">%2</a></li>\n").arg(toHtmlEscaped(url), toHtmlEscaped(name));
    }
    info += QString("</ul>\n");
    eventInfoTextBrowser->setHtml(info);

    // make sure colours are the same as usual
    eventInfoTextBrowser->setPalette(qApp->palette());
    // reduce font size, on maemo
#ifdef MAEMO
    QFont font = eventInfoTextBrowser->font();
    font.setPointSizeF(font.pointSizeF()*0.8);
    eventInfoTextBrowser->setFont(font);
#endif

    connect(favouriteButton, SIGNAL(clicked()), SLOT(favouriteClicked()));
    connect(alarmButton, SIGNAL(clicked()), SLOT(alarmClicked()));

    updateFavouriteButton(event);

    if(event.hasAlarm())
    {
        alarmButton->setIcon(QIcon(":/icons/alarm-on.png"));
    }
}

void EventDialog::favouriteClicked()
{
    Event event = Event::getById(mEventId, mConferenceId);
    event.cycleFavourite();
    event.update("favourite");
    updateFavouriteButton(event);

    // 'conflicts' list may have changed
    QList<Event> conflicts = Event::conflictEvents(event.id(), mConferenceId);

    // have to emit 'eventChanged' signal on all events in conflict
    for(int i=0; i<conflicts.count(); i++)
        emit eventChanged(conflicts[i].id(), false);

    // since the Favourite icon has changed, update TreeViews accordingly
    // all TreeViews have to listen on this signal
    emit eventChanged(event.id(), true);
}

void EventDialog::alarmClicked()
{
    Event event = Event::getById(mEventId, mConferenceId);

    if(event.hasAlarm())
    {
        event.setHasAlarm(false); // update DB
        alarmButton->setIcon(QIcon(":/icons/alarm-off.png"));
#ifdef MAEMO
        // remove alarm from the 'alarmd' alarms list
        Alarm alarm;
        alarm.deleteAlarm(event.conferenceId(), event.id());
        // TODO: test if removing was successfull
#endif /* MAEMO */
    }
    else
    {
        event.setHasAlarm(true);
        alarmButton->setIcon(QIcon(":/icons/alarm-on.png"));
#ifdef MAEMO
        // add alarm to the 'alarmd'
        Alarm alarm;
        alarm.addAlarm(event.conferenceId(), event.id(), event.title(), event.start().addSecs(-AppSettings::preEventAlarmSec()));
#endif /* MAEMO */
    }
    event.update("alarm");
    // since the Alarm icon has changed, update TreeView accordingly
    // all TreeViews have to listen on this signal
    emit eventChanged(event.id(), false);
}


void EventDialog::updateFavouriteButton(const Event& event) {
    switch (event.favourite()) {
        case Favourite_no: favouriteButton->setIcon(QIcon(":/icons/favourite-no.png")); break;
        case Favourite_weak: favouriteButton->setIcon(QIcon(":/icons/favourite-weak.png")); break;
        case Favourite_strong: favouriteButton->setIcon(QIcon(":/icons/favourite-strong.png")); break;
    }
}

