/*
 * Copyright (C) 2010 Ixonos Plc.
 * Copyright (C) 2011-2024 Philipp Spitzer, gregor herrmann, Stefan Stahl
 *
 * This file is part of ConfClerk.
 *
 * ConfClerk is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * ConfClerk is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * ConfClerk.  If not, see <http://www.gnu.org/licenses/>.
 */
#if defined(__GNUC__) || defined(__llvm__) || defined(__clang__)
#include <cxxabi.h>
#endif
#include <exception>

#include "application.h"
#include "errormessage.h"

#include "ormrecord.h"

// if the application uses exceptions,
// there is always a possibility that some will leak uncached from event handler
// crashing the application is too big punishment for it
bool Application::notify(QObject* receiver, QEvent* event)
{
    try {
        return QApplication::notify(receiver, event);
    } catch (OrmException& e) {
        error_message("UNCAUGHT OrmException: " + e.text());
        return false;
    } catch (std::exception& e) {
        error_message("UNCAUGHT exception: " + QString(e.what()));
        return false;
    } catch (...) {
#if defined(__GNUC__) || defined(__llvm__) || defined(__clang__)
        int status = 0;
        char *buff = __cxxabiv1::__cxa_demangle(
                __cxxabiv1::__cxa_current_exception_type()->name(),
                NULL, NULL, &status);
        QString exception_name = QString(buff);
        std::free(buff);
#else
        QString exception_name = QString("unknown");
#endif
        error_message("UNCAUGHT exception: " + exception_name);
        return false;
    }
}
