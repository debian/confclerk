# confclerk.pro

QMAKEVERSION = $$[QMAKE_VERSION]
ISQT4 = $$find(QMAKEVERSION, ^[2-9])
isEmpty( ISQT4 ) {
	error("Use the qmake include with Qt4.7 or greater, on Debian that is qmake-qt4");
}

TEMPLATE = subdirs
SUBDIRS = src

# The global.pri defines the VERSION of the project
include(src/global.pri)

QMAKE_EXTRA_TARGETS += changelog icon man release releaseclean tarball signature

changelog.target = ChangeLog
changelog.commands = \
	git pull && /usr/share/gnulib/build-aux/gitlog-to-changelog > ChangeLog
changelog.CONFIG = phony

icon.target = data/$${TARGET}.png
icon.commands = convert -transparent white data/$${TARGET}.svg data/$${TARGET}.png
icon.depends = data/$${TARGET}.svg

TODAY = $$system(date +%F)
man.target = data/$${TARGET}.1
man.commands = \
	pod2man --utf8 --center=\"Offline conference scheduler\" --release=\"Version $${VERSION}\" --date \"$${TODAY}\" data/$${TARGET}.pod > data/$${TARGET}.1
man.depends = data/$${TARGET}.pod

releaseclean.commands = \
 $(DEL_FILE) data/$${TARGET}.png data/$${TARGET}.1 ChangeLog $${TARGET}-$${VERSION}.tar.gz.asc
release.depends = distclean releaseclean tarball signature
#releaseclean.CONFIG = phony

tarball.target = $${TARGET}-$${VERSION}.tar.gz
tarball.commands = \
	$(DEL_FILE) -r $${TARGET}-$${VERSION} ; \
	$(MKDIR) $${TARGET}-$${VERSION} ; \
	$(COPY_DIR) * $${TARGET}-$${VERSION}/ ; \
	$(DEL_FILE) $${TARGET}-$${VERSION}/*.pro.user* \
		$${TARGET}-$${VERSION}/$${TARGET}-$${VERSION}.tar.gz \
		$(DEL_FILE) -r $${TARGET}-$${VERSION}/$${TARGET}-$${VERSION} \
		$${TARGET}-$${VERSION}/Makefile ; \
	tar -cz --exclude=.svn --exclude=.git --exclude=*.tar.gz -f $$tarball.target $${TARGET}-$${VERSION} ; \
	$(DEL_FILE) -r $${TARGET}-$${VERSION}
tarball.depends = changelog icon man

signature.target = $${TARGET}-$${VERSION}.tar.gz.asc
signature.commands = \
	gpg --armor --detach-sign $${TARGET}-$${VERSION}.tar.gz
signature.depends = tarball
